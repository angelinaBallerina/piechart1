package com.example.piechart

import android.content.Context
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import android.graphics.RectF
import android.R.attr.angle
import android.graphics.Canvas
import android.graphics.Color


class Arc @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0): View(context, attrs, defStyle, defStyleRes) {

    private val START_ANGLE_POINT = 90f
    private lateinit var paint: Paint
    private lateinit var rect: RectF
    var angle: Float = 0.toFloat()


    init {
        val strokeWidth = 40f
        paint = Paint()
        paint.isAntiAlias = true
        paint.style = Paint.Style.STROKE
        paint.strokeWidth = strokeWidth
        paint.color = Color.RED

        rect = RectF(
            strokeWidth.toFloat(),
            strokeWidth.toFloat(),
            (200 + strokeWidth).toFloat(),
            (200 + strokeWidth).toFloat()
        )

        angle = 90f
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas?.drawArc(rect, START_ANGLE_POINT, angle, false, paint)
    }
}