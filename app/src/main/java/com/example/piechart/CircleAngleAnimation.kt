package com.example.piechart

import android.view.animation.Animation
import android.view.animation.Transformation

class CircleAngleAnimation(val arc: Arc, val newAngle: Int) : Animation() {

    private val oldAngle: Float = 0f

    override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
        val angle = arc.angle + (newAngle - arc.angle) * interpolatedTime

        arc.angle = angle
        arc.requestLayout()
    }
}