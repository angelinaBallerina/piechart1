package com.example.piechart

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.PropertyValuesHolder
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View

class PieChart @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
    View(context, attrs, defStyleAttr) {

    private var items: MutableList<Double> = arrayListOf(180.0, 40.0, 48.0, 89.0)
    private val paint = Paint()
    private var whiteCircleRect = RectF()
    private var outerCircleRect = RectF()
    private var startXY = 0f
    var sweepAngle = 0f
    var startAngle = 0f
    private val SWEEP_ANGLE = "sweepAngle"
    private val colors = arrayListOf(
        Color.BLUE,
        Color.parseColor("#EE82EE"),
        Color.parseColor("#4B0082"),
        Color.TRANSPARENT
    )
    var sweepAngles = arrayListOf<Float>()
    var drawnArcs = arrayListOf<ArcData>()
    val drawnCircles = arrayListOf<CircleData>()
    val whiteCircles = arrayListOf<CircleData>()
    private val existingArcsPaint = Paint()
    private val existingCirclesPaint = Paint()
    private val greyCirclePaint = Paint().apply { color = Color.LTGRAY }
    private val whitePaint = Paint().apply { color = Color.WHITE }
    private var circleCenter: Array<Float> = emptyArray()
    val animationStart = 90f

    var arcsToDraw = arrayListOf<ArcData>()

    init {
        paint.isAntiAlias = true
        paint.style = Paint.Style.FILL
        calculateStartAngles(items)

    }

    private fun setDimensions() {
        startXY = width / 2.0f
        outerCircleRect = setRect(outerCircleRect, 1f)
        whiteCircleRect = setRect(whiteCircleRect, 0.8f)
    }

    private fun calculateStartAngles(items: MutableList<Double>) {
        items.forEachIndexed { index, it ->
            val itemSweepAngle: Float = (it * 360f / items.sum()).toFloat()
            val arcStart = sweepAngles.sum() + animationStart
            arcsToDraw.add(
                ArcData(
                    start = if (arcStart > 360f) arcStart % 360 else arcStart,
                    sweepAngle = itemSweepAngle,
                    color = colors[index]
                )
            )
            sweepAngles.add(itemSweepAngle)
        }
    }


    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        setDimensions()

        /*canvas.drawOval(outerCircleRect, greyCirclePaint)

        drawnArcs.forEach {
            canvas.drawArc(
                outerCircleRect,
                it.start,
                it.sweepAngle,
                true,
                existingArcsPaint.apply { color = it.color })
        }
        canvas.drawArc(outerCircleRect, startAngle, sweepAngle, true, paint)
        canvas.drawOval(whiteCircleRect, whitePaint)

        if (circleCenter.isNotEmpty() && circleCenter[0] != startXY && circleCenter[1] != startXY) {
            canvas.drawCircle(circleCenter[0], circleCenter[1], getSmallCircleRadius(), paint)
        }



        canvas.drawCircle(
            startXY,
            startXY + getSmallCircleOffset(),
            getSmallCircleRadius(),
            existingCirclesPaint.apply { color = colors[0] })


        if (drawnCircles.isNotEmpty()) {
            drawnCircles.forEach {
                canvas.drawCircle(
                    it.coords[0],
                    it.coords[1],
                    getSmallCircleRadius(),
                    existingCirclesPaint.apply { color = it.color })
            }
        }*/

        if (whiteCircles.isNotEmpty()) {
            val radius = getSmallCircleRadius()
            whiteCircles.filter { it.coords.isNotEmpty() && (it.coords[0] != 0f || it.coords[1] != 0f) }.forEach {
                /*canvas.drawCircle(
                    it.coords[0],
                    it.coords[1],
                    radius,
                    Paint().apply {
                        color = Color.WHITE
                        strokeWidth = 5f
                        style = Paint.Style.STROKE
                    })
*/
                canvas.drawArc(
                    /*it.coords[0] - radius+ 20f,
                    it.coords[1] + radius+ 20f,
                    it.coords[0] + radius + 20f,
                    it.coords[1] - radius+ 20f,*/

                    50f, 50f, 100f, 100f,
                    it.angle!!,
                    180f,
                    true,
                    Paint().apply {
                        color = Color.BLACK
                        style = Paint.Style.FILL
                    })

            }
        }
    }

    private fun setRect(rectF: RectF, fraction: Float): RectF {
        val radius = width * fraction / 2.0f
        rectF.set(startXY - radius, startXY - radius, startXY + radius, startXY + radius)
        return rectF
    }

    fun animateArc() {
        val animatorSet = AnimatorSet()
        val animators = ArrayList<Animator>()

        arcsToDraw.forEachIndexed { index, arc ->
            val color = arc.color
            val propertySweep = PropertyValuesHolder.ofFloat(SWEEP_ANGLE, 0f, arc.sweepAngle)
            val animator = ValueAnimator().apply {
                setValues(propertySweep)
                duration = 2000L
                addUpdateListener { animation ->
                    paint.color = color
                    sweepAngle = animation.getAnimatedValue(SWEEP_ANGLE) as Float
                    startAngle = arc.start
                    circleCenter = getCircleCenterCoords(startAngle + sweepAngle)
                    invalidate()
                }
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationRepeat(animation: Animator?) {}
                    override fun onAnimationCancel(animation: Animator?) {}
                    override fun onAnimationStart(animation: Animator?) {}
                    override fun onAnimationEnd(animation: Animator?) {
                        drawnArcs.add(arc)
                        drawnCircles.add(CircleData(coords = circleCenter, color = color))
                        whiteCircles.add(
                            CircleData(
                                coords = circleCenter,
                                color = Color.WHITE,
                                angle = startAngle + sweepAngle
                            )
                        )
                    }
                })
            }
            animators.add(animator)
        }
        animatorSet.playSequentially(animators)
        animatorSet.start()
    }

    private fun getCircleCenterCoords(angle: Float): Array<Float> {
        val angle = if (angle > 360) angle - 360 else angle
        val x = startXY + getOffsetX(angle)
        val y = startXY + getOffsetY(angle)
        return arrayOf(x, y)
    }

    private fun getOffsetY(angle: Float): Float {
        val angleRad = angle * Math.PI / 180.0
        return when (angle) {
            in 0f..90f -> Math.sin(angleRad).toFloat() * getSmallCircleOffset()
            in 90f..180f -> Math.sin(Math.PI - angleRad).toFloat() * getSmallCircleOffset()
            in 180f..270f -> -Math.sin(angleRad - Math.PI).toFloat() * getSmallCircleOffset()
            in 270f..360f -> -Math.sin(Math.PI * 2 - angleRad).toFloat() * getSmallCircleOffset()
            else -> 0f
        }
    }

    private fun getOffsetX(angle: Float): Float {
        val angleRad = angle * Math.PI / 180.0
        return when (angle) {
            in 0f..90f -> Math.cos(angleRad).toFloat() * getSmallCircleOffset()
            in 90f..180f -> -Math.cos(Math.PI - angleRad).toFloat() * getSmallCircleOffset()
            in 180f..270f -> -Math.cos(angleRad - Math.PI).toFloat() * getSmallCircleOffset()
            in 270f..360f -> Math.cos(Math.PI * 2 - angleRad).toFloat() * getSmallCircleOffset()
            else -> 0f
        }
    }

    private fun getSmallCircleOffset(): Float = (outerCircleRect.height() + whiteCircleRect.height()) / 4
    private fun getSmallCircleRadius(): Float = (outerCircleRect.height() - whiteCircleRect.height()) / 4
}


data class ArcData(val start: Float, val sweepAngle: Float, val color: Int)
data class CircleData(val coords: Array<Float>, val color: Int, val angle: Float? = null)