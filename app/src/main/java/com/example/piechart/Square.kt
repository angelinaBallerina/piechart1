package com.example.piechart

import android.animation.PropertyValuesHolder
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import java.util.Collections.rotate


class Square @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
    View(context, attrs, defStyleAttr) {

    private var radius: Float = 0f
    private var rotate: Float = 0f
    private val backgroundPaint = Paint().apply {
        color = Color.BLUE
    }


    private val PROPERTY_RADIUS: String = "radius"
    private val PROPERTY_ROTATE: String = "rotate"

    init {

    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        val viewWidth = width / 2f
        val viewHeight = height / 2f

        val leftTopX = viewWidth - 150f
        val leftTopY = viewHeight - 150f

        val rightBotX = viewWidth + 150f
        val rightBotY = viewHeight + 150f
        canvas?.rotate(rotate, viewWidth, viewHeight)
        canvas?.drawRoundRect(leftTopX, leftTopY, rightBotX, rightBotY, radius, radius, backgroundPaint)
    }

    fun animateSquare() {
        val propertyRadius = PropertyValuesHolder.ofFloat(PROPERTY_RADIUS, 0f, 150f)
        val propertyRotate = PropertyValuesHolder.ofFloat(PROPERTY_ROTATE, 0f, 360f)

        val animator = ValueAnimator()
        animator.setValues(propertyRadius, propertyRotate)
        animator.duration = 2000
        animator.addUpdateListener { animation ->
            radius = animation.getAnimatedValue(PROPERTY_RADIUS) as Float
            rotate = animation.getAnimatedValue(PROPERTY_ROTATE) as Float
            invalidate()
        }
        animator.repeatCount = ValueAnimator.INFINITE
        animator.start()
    }
}